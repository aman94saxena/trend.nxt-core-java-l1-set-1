
public class Q6 {
    static int arr[] = {10, 32, 45, 900, 98}; 
    static int findLargest() 
    { 
        int i; 
        int max = arr[0]; 
        for (i = 1; i < arr.length; i++) 
            if (arr[i] > max) 
                max = arr[i]; 
       
        return max; 
    } 
    public static void main(String[] args)  
    { 
        System.out.println("Largest element in given array is " + findLargest()); 
       } 
}