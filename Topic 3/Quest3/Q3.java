import Except.*;

public class Q3 {
    public static void main(String[] args){
        int len = args.length;
        try
        {
            if(len < 5)
                throw new ArrayIndexOutofBoundsException();
        }
        catch (ArrayIndexOutofBoundsException e) {
            System.out.println(e.getMessage());
        }
                int sum=0;
        for(int i=0;i<5;i++){
            sum = sum + Integer.parseInt(args[i]);
        }
        double avg = sum/5.0;
        System.out.println(avg);
    }
}