import automobile.Honda;
import automobile.twowheeler.Hero;
public class Q3 {
    public static void main(String[] args) {
        Hero hero = new Hero("Glamour ", "DL1SW4522", "Aman Saxena", 120);
		hero.getModelName();
		hero.getOwnerName();
		hero.getRegistrationNumber();
		hero.getSpeed();
		hero.radio();
		
		System.out.println();
		
		Honda honda = new Honda("Honda Civic", "UP16C3333", "Yogi Adityanath", 150);
		honda.getModelName();
		honda.getOwnerName();
		honda.getRegistrationNumber();
		honda.getSpeed();
		honda.cdplayer();

	}

}