
public class Q1 {
    int x; 
    boolean isbn;
    String title, author;
    double price;
    public Q1(String t, String a, double p) {
    title = t ;
    author = a;
    price = p ;
    
  }
    public void displaydetails(){
        System.out.println("Book Title :"+this.title+", Author : "+this.author+", Price : "+this.price);
    }
    public double discountedprice(){
        double a = this.price - (this.price*0.20) ;
        return a;
    }
    public static void main(String[] Strings){
        Q1 q = new Q1("The Engineer","Aman",100);
        q.displaydetails();
        System.out.println("Discounted Price : "+q.discountedprice());
    }
}