  class Payment 
{
	protected double amount;
	Payment(double amount) 
	{
			this.amount = Math.round(amount*100)/100.0;
		}
	public double getcash() 
	{
			return amount;
		}
	public void setcash(double newval) 
	{
			this.amount = newval;
		}
	public void paymentDetails() 
	{
			System.out.println("The payment amount: Rs " +this.amount);
		}
    }
class CashPayment extends Payment
    {
    
        CashPayment(double val) 
        {
            super(val);
        }
    
        public void paymentDetails()
        {
            System.out.println("The payment amount : Rs" + this.amount);
        }
    }
class CreditCardPayment extends Payment 
    {
    
        public String name, expDate, number;
    
        CreditCardPayment(double value, String name, String expDate, String number) 
        {
            super(value);
            this.number = number;
            this.expDate = expDate;
            this.name = name;
        }
    
        public void paymentDetails() {
        System.out.println("The payment of Rs" + this.amount + " through the card " + this.number
            + ",  and expire date "	+ this.expDate + ", and the owner name: " + this.name + ".");
        }
    
    }
    
public class Q4 {
    public static void main(String[] args) 
{
        CreditCardPayment a = new CreditCardPayment(1000, "Aman Saxena","16/4/20", "1231245675567");
		CashPayment b = new CashPayment(150);
        CreditCardPayment c = new CreditCardPayment(850, "Narendra  Modi","7/4/20","12341234");
		CashPayment d = new CashPayment(46);
		
		a.paymentDetails();
		b.paymentDetails();
		c.paymentDetails();
		d.paymentDetails();
	}

}