
class Second{
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Finalize method called");
        super.finalize();
    }
}
public class Q6 {
public static void main(String[] args) {
    Second a = new Second();
    a = new Second();
    a = new Second();
    System.gc();
    // try {
    //     a.finalize();
    // } catch (Exception e) {
    //     System.out.println(e.getMessage());
    // }
}
}