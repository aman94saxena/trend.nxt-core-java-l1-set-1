import Quest.*;
public class MainTest {

	public static void main(String[] args) {
		EmployeeDB empDb = new EmployeeDB();
		
		Employee emp1 = new Employee(1, "aman", "aman@w3epic.com", 'M', 250000);
		Employee emp2 = new Employee(2, "modi", "modi@w3epic.com", 'M', 30000);
		Employee emp3 = new Employee(3, "John", "john@w3epic.com", 'M', 2000);
		Employee emp4 = new Employee(4, "cena", "cena@w3epic.com", 'F', 5000);
		
		empDb.addEmployee(emp1);
		empDb.addEmployee(emp2);
		empDb.addEmployee(emp3);
		empDb.addEmployee(emp4);

		for (Employee emp : empDb.listAll())
			System.out.println(emp.GetEmployeeDetails());
		
		System.out.println();
		empDb.deleteEmployee(102);
		
		for (Employee emp : empDb.listAll())
			System.out.println(emp.GetEmployeeDetails());
		
		System.out.println();
		
		System.out.println(empDb.showPaySlip(2));
	}

}