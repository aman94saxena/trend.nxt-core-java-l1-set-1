import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
public class Q4 {
    HashMap<String,Integer> map = new HashMap<>();

    public void addContact(String name, Integer number) {
        map.put(name, number);    
    }
    @SuppressWarnings("unchecked")  
    public void getphoneNumber(String name) {
        if (map.containsKey(name)) {
            Integer a = Integer.parseInt(map.get(name).toString());
            System.out.println("Contact of " +name+" is " + a);
        }
    }
    public static void main(String[] args) {
        Q4 a4 = new Q4();
        a4.addContact("aman", 10345);
        a4.addContact("sachin", 30456);
        a4.addContact("modi", 30458);
        Scanner s=new Scanner(System.in);
        System.out.println("Enter name to get contact details");
        a4.getphoneNumber(s.next());
        s.close();
    }
}